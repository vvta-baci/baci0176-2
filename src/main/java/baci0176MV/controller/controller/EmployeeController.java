package baci0176MV.controller.controller;

import baci0176MV.controller.model.Employee;
import baci0176MV.controller.repository.EmployeeRepositoryInterface;
import baci0176MV.controller.view.EmployeeView;

import java.util.List;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeView employeeView;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
		this.employeeView = new EmployeeView();
	}
	
	public void addEmployee(Employee employee) {
		employeeRepository.addEmployee(employee);
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(Employee oldEmployee, DidacticFunction newFunction) {
		employeeRepository.modifyEmployeeFunction(oldEmployee, newFunction);
	}

	public void removeEmployee(int id) {
		employeeRepository.removeEmployee(id);
	}
	
	public List<Employee> getSortedEmployeeList() {
		return employeeRepository.getEmployeeByCriteria();
	}
	
	public void printMenu() {
		employeeView.printMenu();
	}

	public Employee findEmployeeById(int idOldEmployee) {
		return employeeRepository.findEmployeeById(idOldEmployee);
	}

}
