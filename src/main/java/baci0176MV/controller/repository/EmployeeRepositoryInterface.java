package baci0176MV.controller.repository;

import baci0176MV.controller.controller.DidacticFunction;
import baci0176MV.controller.model.Employee;

import java.util.List;

public interface EmployeeRepositoryInterface {
	boolean addEmployee(Employee employee);
	void modifyEmployeeFunction(Employee employee, DidacticFunction newFunction);
	List<Employee> getEmployeeList();
	List<Employee> getEmployeeByCriteria();
	Employee findEmployeeById(int idOldEmployee);
	boolean removeEmployee(int id);
}
