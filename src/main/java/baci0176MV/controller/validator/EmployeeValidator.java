package baci0176MV.controller.validator;

import baci0176MV.controller.controller.DidacticFunction;
import baci0176MV.controller.model.Employee;

public class EmployeeValidator {
	
	public EmployeeValidator(){}

	public boolean isValid(Employee employee) {
		return isFirstNameValid(employee) 
				&& isLastNameValid(employee) 
				&& isCnpValid(employee) 
				&& isFunctionValid(employee) 
				&& isSalaryValid(employee);
	}

	private boolean isSalaryValid(Employee employee) {
		return employee.getSalary() > 0;
	}

	private boolean isFunctionValid(Employee employee) {
		return employee.getFunction().equals(DidacticFunction.ASISTENT)
				|| employee.getFunction().equals(DidacticFunction.LECTURER)
				|| employee.getFunction().equals(DidacticFunction.TEACHER)
				|| employee.getFunction().equals(DidacticFunction.CONFERENTIAR);
	}

	private boolean isCnpValid(Employee employee) {
		return employee.getCnp().matches("[0-9]{13}");
	}

	private boolean isLastNameValid(Employee employee) {
		return employee.getLastName().matches("\\p{Lu}\\pL{2,}");
	}

	private boolean isFirstNameValid(Employee employee) {
		return employee.getFirstName().matches("\\p{Lu}\\pL{2,}");
	}
	
}
