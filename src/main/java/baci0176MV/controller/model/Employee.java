package baci0176MV.controller.model;

import baci0176MV.controller.controller.DidacticFunction;

public class Employee {
	private int id;
	private String nume;/** The last name of the employee */
	private String firstName; /** The first name of the employee */
	private String cnp; /** The unique id of the employee */
	private DidacticFunction function; /** The didactic function of the employee inside the university */
	private Double salary; /** The salary of the employee */
	
	/**
	 * Default constructor for employee
	 */
	public Employee() {
		this.firstName = "";
		this.nume  = "";
		this.cnp       = "";
		this.function  = DidacticFunction.ASISTENT;
		this.salary    = 0.0d;
	}
	
	/**
	 * Constructor with fields for employee
	 */
	public Employee(String firstName, String lastName, String cnp, DidacticFunction function, Double salary) {
		this.firstName = firstName;
		this.nume  = lastName;
		this.cnp       = cnp;
		this.function  = function;
		this.salary    = salary;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return nume;
	}

	public void setLastName(String lastName) {
		this.nume = lastName;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public DidacticFunction getFunction() {
		return function;
	}

	public void setFunction(DidacticFunction function) {
		this.function = function;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}
	
	@Override
	public String toString() {
		String employee = "";
		employee += firstName + ";";
		employee += nume + ";";
		employee += cnp + ";";
		employee += function.toString() + ";";
		employee += salary + ";";
		employee += id;
		return employee;
	}
	
	@Override
	public boolean equals(Object otherEmployee) {
		if (otherEmployee == this) {
            return true;
        }
		if (!(otherEmployee instanceof Employee)) {
            return false;
        }
		final Employee employee = (Employee) otherEmployee;
		boolean hasSameFirstName = this.firstName.equals(employee.getFirstName()),
				hasSameLastName  = this.nume.equals(employee.getLastName()),
				hasSameCNP       = this.cnp.equals(employee.getCnp()),
				hasSameFunction  = this.function.equals(employee.getFunction()),
				hasSameSalary    = this.salary.equals(employee.getSalary());
		return hasSameFirstName && hasSameLastName && hasSameCNP && hasSameFunction && hasSameSalary;
	}
	
	@Override
	public int hashCode() {
		return this.hashCode();
	}
	
}
