package baci0176MV.repository;

import baci0176MV.controller.DidacticFunction;
import baci0176MV.model.Employee;
import baci0176MV.validator.EmployeeException;
import baci0176MV.validator.EmployeeValidator;

public class EmployeeBuilder {
    public static final int LAST_NAME_INDEX = 1;
    public static final int FIRST_NAME_INDEX = 0;
    public static final int CNP_INDEX = 2;
    public static final int DIDACTIC_FUNCTION_INDEX = 3;
    public static final int SALARY_INDEX = 4;

    /**
     * Returns the Employee after parsing the given line
     *
     * @param employeeString
     *            the employee given as String from the input file
     * @param line
     *            the current line in the file
     *
     * @return if the given line is valid returns the corresponding Employee
     * @throws EmployeeException
     */

    public static Employee getEmployeeFromString(String employeeString, int line) throws EmployeeException {
        Employee employee = new Employee();

        String[] attributes = employeeString.split("[;]");

        if (attributes.length != 6) {
            throw new EmployeeException("Invalid line at: " + line);
        } else {
            EmployeeValidator validator = new EmployeeValidator();
            employee.setFirstName(attributes[FIRST_NAME_INDEX]);
            employee.setLastName(attributes[LAST_NAME_INDEX]);
            employee.setCnp(attributes[CNP_INDEX]);

            if (attributes[DIDACTIC_FUNCTION_INDEX].equals("ASISTENT"))
                employee.setFunction(DidacticFunction.ASISTENT);
            if (attributes[DIDACTIC_FUNCTION_INDEX].equals("LECTURER"))
                employee.setFunction(DidacticFunction.LECTURER);
            if (attributes[DIDACTIC_FUNCTION_INDEX].equals("TEACHER"))
                employee.setFunction(DidacticFunction.TEACHER);
            if (attributes[DIDACTIC_FUNCTION_INDEX].equals("CONFERENTIAR"))
                employee.setFunction(DidacticFunction.CONFERENTIAR);

            employee.setSalary(Double.valueOf(attributes[SALARY_INDEX]));
            //employee.setId(Integer.valueOf(attributes[ID]));
            employee.setId(Integer.valueOf(line));

            if (!validator.isValid(employee)) {
                throw new EmployeeException("Invalid line at: " + line);
            }
        }

        return employee;
    }
}
