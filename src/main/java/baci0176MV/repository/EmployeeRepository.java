package baci0176MV.repository;

import baci0176MV.controller.DidacticFunction;
import baci0176MV.model.AgeCriteria;
import baci0176MV.model.Employee;
import baci0176MV.model.SalaryCriteria;
import baci0176MV.validator.EmployeeException;
import baci0176MV.validator.EmployeeValidator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmployeeRepository implements EmployeeRepositoryInterface {

	private static final String ERROR_WHILE_READING_MSG = "Error while reading: ";
	private final String employeeDBFile = "employeeDB/employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();
	private List<Employee> employeeList = new ArrayList<>();
	
	public EmployeeRepository() {
		employeeList = loadEmployeesFromFile();
	}

	@Override
	public boolean addEmployee(Employee employee) {
		for (Employee e : employeeList) {
			if (e.equals(employee)) {
				return false;
			}
		}

		employee.setId(employeeList.size());
		if (employeeValidator.isValid(employee)) {
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(employeeDBFile, true))) {
				bw.write(employee.toString());
				bw.newLine();
				employeeList.add(employee);
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public boolean removeEmployee(int id) {
		try {
			employeeList.remove(id);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(employeeDBFile), StandardOpenOption.TRUNCATE_EXISTING)) {
			for (Employee employee : employeeList) {
				bw.write(employee.toString());
				bw.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public void modifyEmployeeFunction(Employee oldEmployee, DidacticFunction newFunction) {
		oldEmployee.setFunction(newFunction);
	}
	
	private List<Employee> loadEmployeesFromFile() {
		final List<Employee> employeeList = new ArrayList<Employee>();
		try (BufferedReader br = new BufferedReader(new FileReader(employeeDBFile));){
			String line;
			int lineNumber = 0;
			while ((line = br.readLine()) != null) {
				readEmployeeFromLine(employeeList, line, lineNumber);
				lineNumber++;
			}
		} catch (IOException e) {
			System.err.println(ERROR_WHILE_READING_MSG + e);
		} 
		return employeeList;
	}

	private void readEmployeeFromLine(List<Employee> employeeList, String line, int lineNumber) {
		try {
			final Employee employee = EmployeeBuilder.getEmployeeFromString(line, lineNumber);
			employeeList.add(lineNumber, employee);
		} catch (EmployeeException ex) {
			System.err.println(ERROR_WHILE_READING_MSG + ex.toString());
		}
	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@Override
	public List<Employee> getEmployeeByCriteria() {
		List<Employee> employeeSortedList = new ArrayList<Employee>(employeeList);
		Collections.copy(employeeSortedList, employeeList);
		Collections.sort(employeeSortedList, new AgeCriteria());
		//System.out.println(employeeSortedList);
		Collections.sort(employeeSortedList, new SalaryCriteria());
		//System.out.println(employeeSortedList);
		return employeeSortedList;
	}

	@Override
	public Employee findEmployeeById(int idOldEmployee) {
		for (Employee employee : employeeList) {
			if (employee.getId() == idOldEmployee) {
				return employee;
			}
		}
		return null;
	}

}
