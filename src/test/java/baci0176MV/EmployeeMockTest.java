package baci0176MV;

import baci0176MV.controller.DidacticFunction;
import baci0176MV.model.Employee;
import baci0176MV.repository.EmployeeMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

class EmployeeMockTest {
    List<Employee> employeeList;

    @BeforeEach
    void setUp() {
        employeeList = new ArrayList<>();
        Employee e1 = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        Employee e2 = new Employee("Ion", "Dumitrescu", "1234567890876", DidacticFunction.LECTURER, 2500d);
        Employee e3 = new Employee("Gicu", "Ionescu", "1234567890876", DidacticFunction.LECTURER, 2500d);
        Employee e4 = new Employee("Dodel", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        e1.setId(1);
        e2.setId(2);
        e3.setId(3);
        e4.setId(4);

        employeeList.add(e1);
        employeeList.add(e2);
        employeeList.add(e3);
        employeeList.add(e4);
    }

    @Test
    void modifyFunction_TC01_existingEmployee() {
        EmployeeMock mock = new EmployeeMock(employeeList);
        mock.modifyEmployeeFunction(employeeList.get(3), DidacticFunction.LECTURER);
        assertSame(DidacticFunction.LECTURER, employeeList.get(3).getFunction());
    }

    @Test
    void modifyFunction_TC02_emptyList() {
        List<Employee> employeeList = new ArrayList<Employee>();
        Employee e = new Employee("Dodel", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        EmployeeMock mock = new EmployeeMock(employeeList);
        mock.modifyEmployeeFunction(e, DidacticFunction.LECTURER);
        assertSame(DidacticFunction.ASISTENT, e.getFunction());
    }

    @Test
    void modifyFunction_TC03_nullEmployee() {
        List<Employee> copy = new ArrayList<>(employeeList);
        EmployeeMock mock = new EmployeeMock(employeeList);
        mock.modifyEmployeeFunction(null, DidacticFunction.LECTURER);
        assertEquals(employeeList, copy);
    }

    @Test
    void modifyFunction_TC04_missingEmployee() {
        List<Employee> copy = new ArrayList<>(employeeList);
        EmployeeMock mock = new EmployeeMock(employeeList);
        Employee e = new Employee("Mia", "Popescu", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        mock.modifyEmployeeFunction(e, DidacticFunction.LECTURER);
        assertEquals(employeeList, copy);
    }
}