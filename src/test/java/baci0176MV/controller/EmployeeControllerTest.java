package baci0176MV.controller;

import baci0176MV.model.Employee;
import baci0176MV.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeControllerTest {

    private EmployeeRepository repository;

    @BeforeEach
    void setUp() {
        this.repository = new EmployeeRepository();
    }

    @Test
    void addEmployee_TC1_BB() {
        Employee employee = new Employee(
                "Andra",
                "Barsoianu",
                "1234567890123",
                DidacticFunction.ASISTENT,
                2.0
        );

        assertTrue(repository.addEmployee(employee));
    }

    @Test
    void addEmployee_TC2_BB() {
        Employee employee = new Employee(
                "Irina",
                "Barsoianu",
                "1234567890123",
                DidacticFunction.ASISTENT,
                0.0
        );

        assertFalse(repository.addEmployee(employee));
    }

    @Test
    void addEmployee_TC3_BB() {
        Employee employee = new Employee(
                "ioana",
                "Barsoianu",
                "1234567890123",
                DidacticFunction.ASISTENT,
                0.0
        );

        assertFalse(repository.addEmployee(employee));
    }

    @Test
    void addEmployee_TC4_BB() {
        Employee employee = new Employee(
                "Andra",
                "Barsoianu",
                "1234567890123",
                DidacticFunction.ASISTENT,
                2.0
        );

        assertFalse(repository.addEmployee(employee));
    }


    @Test
    void addEmployee_TC5_BB() {
        Employee employee = new Employee(
                "@ndra",
                "Barsoianu",
                "1234567890123",
                DidacticFunction.ASISTENT,
                2.0
        );

        assertFalse(repository.addEmployee(employee));
    }

    @Test
    void addEmployee_TC6_BB() {
        Employee employee = new Employee(
                "Zelda",
                "Barsoianu",
                "1234567890123",
                DidacticFunction.ASISTENT,
                2.0
        );

        assertTrue(repository.addEmployee(employee));
    }

    @Test
    void addEmployee_TC7_BB() {
        Employee employee = new Employee(
                "]ndra",
                "Barsoianu",
                "1234567890123",
                DidacticFunction.ASISTENT,
                2.0
        );

        assertFalse(repository.addEmployee(employee));
    }

    @Test
    void addEmployee_TC8_BB() {
        Employee employee = new Employee(
                "Maria",
                "Barsoianu",
                "1234567890123",
                DidacticFunction.ASISTENT,
                2.0
        );

        assertTrue(repository.addEmployee(employee));
    }
}
